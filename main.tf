# Set remote backend
terraform {
  required_version = ">= 0.13"
  required_providers {
    hcloud = {
      source = "terraform-providers/hcloud"
    }
  }
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "GraphIP"

    workspaces {
      name = "testing"
    }
  }
}

# Variables stored in Terraform Cloud
variable "hcloud_token" {}
variable "ssh_key_name" {}
variable "s3_endpoint" {}
variable "s3_access_key" {}
variable "s3_secret_key" {}
variable "minio_endpoint" {}
variable "minio_access_key" {}
variable "minio_secret_key" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

# Select SSH Key stored in Hetzner Cloud
data "hcloud_ssh_key" "test-key" {
  name = var.ssh_key_name
}

# Create a server
resource "hcloud_server" "node1" {
  name = "node1"
  image = "ubuntu-18.04"
  user_data = templatefile("user_data.yaml", {
    s3_endpoint = var.s3_endpoint
    s3_access_key = var.s3_access_key
    s3_secret_key = var.s3_secret_key
    minio_endpoint = var.minio_endpoint
    minio_access_key = var.minio_access_key
    minio_secret_key = var.minio_secret_key
    file_name = var.ssh_key_name #pass variable to user_data
    file_id = data.hcloud_ssh_key.test-key.id #pass variable to user_data
  })
  server_type = "ccx41"
  location = "hel1"
  ssh_keys  = [data.hcloud_ssh_key.test-key.id]
}
